'''
Created on Feb 9, 2019

@author: coditum
'''
from random import randint

r = randint(0,100)
Lives=6
while(Lives>0):
    Try1=int(input("Guess a number between 0 and 100. You have " + str(Lives) + " tries left"))
    if (Try1 == r):
        print("You Are Right! You Had " + str(Lives) + " lives left")
        if(Lives ==6):
            print("You Are MAGICAL! You Got It First Try")
        break
    elif(Try1<r):
        print("The number is higher")
    elif(Try1>r):
        print("The number is lower")
    Lives-=1
    if(Lives ==0):
        print("GAME OVER")
    